/************************************************************************************
 * include/board.h
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __INCLUDE_BOARD_H
#define __INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#ifndef __ASSEMBLY__
# include <stdint.h>
#endif

#include <stm32l4.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/

#if defined(CONFIG_ARCH_CHIP_STM32L476RG)
#  include <arch/board/grxbetastamp.h>
#endif

/* DMA Values defined in arch/arm/src/stm32l4/chip/stm32l4x6xx_dma.h */

/*--------------------*/
/* UART CONFIGURATION */
/*--------------------*/

/* UART1 - no clock (P11) RX(PB7) TX(PB6) */

#define GPIO_USART1_RX    GPIO_USART1_RX_2
#define GPIO_USART1_TX    GPIO_USART1_TX_2
#define DMACHAN_USART1_RX DMACHAN_USART1_RX_1

/* UART2 - with clock (P14) RX(PA3) TX(PA2) CK(PA4) */

#define GPIO_USART2_RX   GPIO_USART2_RX_1
#define GPIO_USART2_TX   GPIO_USART2_TX_1
/* No DMA line choice */

/* UART3 - with clock (P13) RX(PC5) TX(PC4) CK(PB12) */

#define GPIO_USART3_RX   GPIO_USART3_RX_2
#define GPIO_USART3_TX   GPIO_USART3_TX_2
#define GPIO_USART3_CK   GPIO_USART3_CK_2
/* No DMA line choice */

/* UART4 (P9, RS485) RX(PA1) TX(PA0) EN(PC3) */
/* Transceiver : */

#define GPIO_UART4_RX   GPIO_UART4_RX_1
#define GPIO_UART4_TX   GPIO_UART4_TX_1
#define GPIO_UART4_RS485_DIR (GPIO_PORTC|GPIO_PIN3|GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
/* No DMA line choice */

/* UART5 Not available */

/*-------------------*/
/* SPI CONFIGURATION */
/*-------------------*/

/*SPI1 P(15) MISO(PB4) MOSI(PB5) SCLK(PB3) */
#define GPIO_SPI1_MISO   GPIO_SPI1_MISO_2
#define GPIO_SPI1_MOSI   GPIO_SPI1_MOSI_2
#define GPIO_SPI1_SCK    GPIO_SPI1_SCK_2
#define DMACHAN_SPI1_RX  DMACHAN_SPI1_RX_1 /* 2 choices */
#define DMACHAN_SPI1_TX  DMACHAN_SPI1_TX_1 /* 2 choices */

/*SPI2 (On board peripherals) MISO(PB14) MOSI(PB15) SCLK(PB13) */
#define GPIO_SPI2_MISO   GPIO_SPI2_MISO_1
#define GPIO_SPI2_MOSI   GPIO_SPI2_MOSI_1
#define GPIO_SPI2_SCK    GPIO_SPI2_SCK_2
/* No DMA line choice */

/*SPI3 P(17) MISO(PC11) MOSI(PC12) SCLK(PC10) */
#define GPIO_SPI3_MISO   GPIO_SPI3_MISO_2
#define GPIO_SPI3_MOSI   GPIO_SPI3_MOSI_2
#define GPIO_SPI3_SCK    GPIO_SPI3_SCK_2
/* No DMA line choice */

/*--------------------*/
/* QSPI CONFIGURATION */
/*--------------------*/

/*QSPI CLK(PB10) NCS(PB11) IO0(PB1) IO1(PB0) IO2(PA7) IO3(PA6)*/
#define GPIO_QUADSPI_CLK GPIO_QUADSPI_CLK_1
#define GPIO_QUADSPI_NCS GPIO_QUADSPI_NCS_1
#define GPIO_QUADSPI_IO0 GPIO_QUADSPI_BK1_IO0_1
#define GPIO_QUADSPI_IO1 GPIO_QUADSPI_BK1_IO1_1
#define GPIO_QUADSPI_IO2 GPIO_QUADSPI_BK1_IO2_1
#define GPIO_QUADSPI_IO3 GPIO_QUADSPI_BK1_IO3_1
#define DMACHAN_QUADSPI  DMACHAN_QUADSPI_1 /* 2 choices */

/*-------------------*/
/* I2C CONFIGURATION */
/*-------------------*/

/* I2C1 (P10) SDA(PB8) SCL(PB9) */

#define GPIO_I2C1_SDA    GPIO_I2C1_SDA_2
#define GPIO_I2C1_SCL    GPIO_I2C1_SCL_2
#define DMACHAN_I2C1_RX  DMACHAN_I2C1_RX_1
#define DMACHAN_I2C1_TX  DMACHAN_I2C1_TX_1

/* I2C2 Not available */

/* I2C3 (P8) SDA(PC1) SCL(PC0) */

#define GPIO_I2C3_SDA    GPIO_I2C3_SDA_1
#define GPIO_I2C3_SCL    GPIO_I2C3_SCL_1
/* No DMA line choice */

/*-------------------*/
/* CAN CONFIGURATION */
/*-------------------*/

/* CAN1 (P16) TX(PA12) RX(PA11) */
/* Using SN65HVD233 transceiver on power board */
/* Pins shared with OTGFS */
#define GPIO_CAN1_TX     GPIO_CAN1_TX_1
#define GPIO_CAN1_RX     GPIO_CAN1_RX_1

/*----------------------*/
/* SPI CS CONFIGURATION */
/*----------------------*/

#define GPIO_CS_EEPROM (GPIO_PORTA|GPIO_PIN10|GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_FLASH  (GPIO_PORTC|GPIO_PIN6 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_USER0  (GPIO_PORTA|GPIO_PIN9 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_USER1  (GPIO_PORTA|GPIO_PIN8 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_USER2  (GPIO_PORTC|GPIO_PIN9 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_USER3  (GPIO_PORTC|GPIO_PIN8 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
#define GPIO_CS_USER4  (GPIO_PORTC|GPIO_PIN7 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)

/*-------------------*/
/* LED CONFIGURATION */
/*-------------------*/

/* To save energy when the LED is saved, LEDs are wired open drain:
 *   - When the I/O is LOW, the LED is on.
 *   - When the I/O is HIGH or FLOATING, the LED is off. Floating is preferred to save current.
 */

#define GPIO_LED_BOT (GPIO_PORTD|GPIO_PIN2 |GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)
#define GPIO_LED_TOP (GPIO_PORTA|GPIO_PIN15|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)

/* LED index values for use with board_userled() */

#define BOARD_LED_BOT     0
#define BOARD_LED_TOP     1
#define BOARD_NLEDS       2

/* LED bits for use with board_userled_all() */

#define BOARD_LED_TOP_BIT  (1 << BOARD_LED_TOP)
#define BOARD_LED_BOT_BIT  (1 << BOARD_LED_BOT)

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/
/************************************************************************************
 * Name: stm32l4_board_initialize
 *
 * Description:
 *   All STM32L4 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32l4_board_initialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __INCLUDE_BOARD_H */
